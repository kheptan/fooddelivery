# FoodDelivery

# Add new User
{ 
	"name" : "marian",
	"phone" : 129712,
	"address": "pompiliu 35"
}

# Add new Category with Products
{
	"name" : "pizza",
	"products" : [
			{
				"name" : "pizza margherita",
				"price" : 12.90
			},
			{
				"name" : "pizza romana",
				"price" : 11.10
			},
			{
				"name" : "pizza prosciuto",
				"price" : 12.0
			}
	]
}

# Add new Restaurant
{
	"name" : "origo",
	"address":"alexandri mihnea",
	"categories": [1]
}

# Add new Order

{
	"status" : "CONFIRMED",
	"client" : {
				"id": 1
		},
	"restaurant" : {
				"id" : 1	
		},
	"details" : [
		{
			"product" : { "id" : 1 },
			"qty" : 10
		},
		{
			"product" : { "id" : 2 },
			"qty" : 2
		}
	]		
}

# Show Total Order
{
    "id": 1,
    "client": "marian",
    "restaurant": "origo",
    "total": 142.2
}



